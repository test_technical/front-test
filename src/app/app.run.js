angular.module('app')
.run(['$rootScope','$location', 'editableOptions', 'editableThemes', 
  function ($rootScope, $location, editableOptions, editableThemes) {
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
    editableThemes['bs3'].submitTpl = '<button class="btn btn-flat btn-primary" type="submit">'+
                                       '<i class="fa fa-check"></i></button>';

    editableThemes['bs3'].cancelTpl = '<button class="btn btn-flat btn-default" ng-click="$form.$cancel()">'+
                                       '<i class="fa fa-times"></i></button>';

    $rootScope.goTo = function(url){
      $location.path(url); 
    };

}]);
