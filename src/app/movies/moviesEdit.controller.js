
angular.module('app.movies')

.controller('MoviesEditCtrl', ['$scope', 'MoviesService','$rootScope','$routeParams', function($scope, MoviesService,$rootScope,$routeParams) {

$scope.movie = null;

    $scope.getMovies= function(){  
      MoviesService.getOne($routeParams.id,$scope.success, $scope.error);
    }
    $scope.success = function(response){
      response.data.year = new Date(response.data.year)
      $scope.movie = response.data;
      console.log($scope.movie)
    };
    $scope.error = function(error){
      console.log(error);
    };

$scope.getMovies();

$scope.edit = function(movie){
    movie.year = movie.year.toString()
    MoviesService.edit(movie, $scope.success1, $scope.error1);
  }
    $scope.success1 = function(response){
      if(response.status==200){
        swal(
          'Editado Exitosamente!',
          '',
          'success'
        )
      }
      $rootScope.goTo('/movies')
      console.log(response)
    };

    $scope.error1 = function(error){
      swal(
          'Error al editar !',
          'intente nuevamente',
          'error'
        )
      console.log(error);
    };
 
    


}]);
