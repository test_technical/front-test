angular.module('app.movies', [])

.service('MoviesService', ['$http', 'CONFIG', function($http,  CONFIG){
   var url = CONFIG.API_URL+'movies';

    this.handleError = function(res){
      console.warn(res);
    };

    this.get = function(callback, error){
    	var config = {
	        method : 'GET',
	        url    : url,
	        headers: {
	          'Content-Type' : 'application/json'
	        }
	      };
      return $http(config).then(callback, error);
    };

    this.getOne = function(id,callback, error){
      var config = {
          method : 'GET',
          url    : url+'/'+id,
          headers: {
            'Content-Type' : 'application/json'
          }
        };
      return $http(config).then(callback, error);
    };

   this.create = function(data,callback, error){
    var config = {
        method : 'POST',
        url    : url,
        data   :data,
        headers: {
          'Content-Type' : 'application/json'
        }
      };
    return $http(config).then(callback, error);
  };

   this.edit = function(data,callback, error){
    var config = {
        method : 'PUT',
        url    : url+'/'+data._id,
        data   :data,
        headers: {
          'Content-Type' : 'application/json'
        }
      };
    return $http(config).then(callback, error);
  };
  this.delete = function(id,callback, error){
      var config = {
          method : 'DELETE',
          url    : url+'/'+id,
          headers: {
            'Content-Type' : 'application/json'
          }
        };
      return $http(config).then(callback, error);
    };
}]);
