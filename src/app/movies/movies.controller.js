
angular.module('app.movies')

.controller('MoviesCtrl', ['$scope', 'MoviesService','$rootScope', function($scope, MoviesService,$rootScope) {

  $scope.movies = null;


  	$scope.getMovies= function(){
     
  		MoviesService.get($scope.success, $scope.error);
  	}

  	$scope.success = function(response){
  		$scope.movies = response.data;
  	};

  	$scope.error = function(error){
  		console.log(error);
  	};

 	$scope.getMovies();

  $scope.delete = function(id,index){
    $scope.indexA = index;
    MoviesService.delete(id,$scope.success1, $scope.error1);
    console.log(id)
  }
  $scope.success1 = function(response){
      if(response.status==200){
        $scope.movies.splice($scope.indexA, 1);
        swal(
          'Eliminado Exitosamente!',
          '',
          'success'
        )
      }
      $rootScope.goTo('/movies')
      console.log(response)
    };

    $scope.error1 = function(error){
      swal(
          'Error al eliminar !',
          'intente nuevamente',
          'error'
        )
      console.log(error);
    };

}]);
