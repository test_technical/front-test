
angular.module('app.movies')

.controller('MoviesCreateCtrl', ['$scope', 'MoviesService','$rootScope', function($scope, MoviesService,$rootScope) {

  $scope.create = function(movie){
    MoviesService.create(movie, $scope.success, $scope.error);
  }
    $scope.success = function(response){
      if(response.status==200){
        swal(
          'Creado Exitosamente!',
          '',
          'success'
        )
      }
      $rootScope.goTo('/movies')
      console.log(response)
    };
    $scope.error = function(error){
      swal(
          'Error al crear !',
          'intente nuevamente',
          'error'
        )
      console.log(error);
    };

    


}]);
