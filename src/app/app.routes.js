
/*
 * url routes
 */
  angular.module('app') 
  .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    'use strict';
    $routeProvider
    .when("/", {
      controller: "HomeCtrl",
      templateUrl: "app/home/home.html"
    })
    .when("/movies", {
      controller: "MoviesCtrl",
      templateUrl: "app/movies/movies.html"
    })
    .when("/movies/create", {
      controller: "MoviesCreateCtrl",
      templateUrl: "app/movies/movies_create.html"
    })
    .when("/movies/edit/:id", {
      controller: "MoviesEditCtrl",
      templateUrl: "app/movies/movies_edit.html"
    })
    .when("/theaters", {
      controller: "TheaterCtrl",
      templateUrl: "app/theaters/theaters.html"
    })
    .when("/theater/create", {
      controller: "TheaterCreateCtrl",
      templateUrl: "app/theaters/theater_create.html"
    })
    .when("/theater/edit/:id", {
      controller: "TheaterEditCtrl",
      templateUrl: "app/theaters/theater_edit.html"
    })
    .otherwise({
      redirectTo: '/'
    });
    $locationProvider.html5Mode(true);
  }]);

