
angular.module('app.theaters')

.controller('TheaterCtrl', ['$scope', 'TheaterService','$rootScope','MoviesService', function($scope, TheaterService,$rootScope,MoviesService) {

 console.log('hello theater')
 
 $scope.theaters = null;
 
  	$scope.getTheaters= function(){
     
  		TheaterService.get($scope.success, $scope.error);
  	}


  	$scope.success = function(response){
  		$scope.theaters = response.data;
  		console.log($scope.theaters)
  	};

  	$scope.error = function(error){
  		console.log(error);
  	};

 	$scope.getTheaters();

  $scope.delete = function(id,index){
    $scope.indexA = index;
    TheaterService.delete(id,$scope.success1, $scope.error1);
  }
  $scope.success1 = function(response){
      if(response.status==200){
        $scope.theaters.splice($scope.indexA, 1);
        swal(
          'Eliminado Exitosamente!',
          '',
          'success'
        )
      }
      $rootScope.goTo('/theaters')
      console.log(response)
    };

    $scope.error1 = function(error){
      swal(
          'Error al eliminar !',
          'intente nuevamente',
          'error'
        )
      console.log(error);
    };

}]);
