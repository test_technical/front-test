
angular.module('app.theaters')

.controller('TheaterCreateCtrl', ['$scope', 'TheaterService','$rootScope','MoviesService', function($scope, TheaterService,$rootScope,MoviesService) {


  MoviesService.get(function(movies){
        $scope.values = movies.data;
        console.log($scope.movies)
  });
  $scope.create = function(theater){
    theater.movies = $scope.moviesSelectes;
    TheaterService.create(theater, $scope.success, $scope.error);
  }
    $scope.success = function(response){
      if(response.status==200){
        swal(
          'Creado Exitosamente!',
          '',
          'success'
        )
      }
      $rootScope.goTo('/theaters')
      console.log(response)
    };
    $scope.error = function(error){
      swal(
          'Error al crear !',
          'intente nuevamente',
          'error'
        )
      console.log(error);
    };

    


}]);
