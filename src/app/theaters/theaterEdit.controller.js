
angular.module('app.theaters')

.controller('TheaterEditCtrl', ['$scope', 'TheaterService','$rootScope','$routeParams','MoviesService', function($scope, TheaterService,$rootScope,$routeParams,MoviesService) {

$scope.theater = null;
 $scope.movies = null;

    $scope.getTheater= function(){  
      TheaterService.getOne($routeParams.id,function(response){
        console.log(response)
        $scope.theater = response.data;
        MoviesService.get(function(movies){
        $scope.values = movies.data;
        console.log($scope.movies)
        });
      });
    }

$scope.getTheater();

$scope.edit = function(theater){
  console.log(theater)
  console.log($scope.moviesSelectes)
  theater.movies = $scope.moviesSelectes;
    TheaterService.edit(theater, $scope.success1, $scope.error1);
  }
    $scope.success1 = function(response){
      if(response.status==200){
        swal(
          'Editado Exitosamente!',
          '',
          'success'
        )
      }
      $rootScope.goTo('/theaters')
      console.log(response)
    };

    $scope.error1 = function(error){
      swal(
          'Error al editar !',
          'intente nuevamente',
          'error'
        )
      console.log(error);
    };
 
    


}]);
