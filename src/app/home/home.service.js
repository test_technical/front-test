angular.module('app.home', [])

.service('HomeService', ['$http', 'CONFIG', function($http,  CONFIG){
    var urlMovies = CONFIG.API_URL+'movies';
    var urlTheater = CONFIG.API_URL+'theater';

    this.handleError = function(res){
      console.warn(res);
    };

    this.get = function(callback, error){
    	var config = {
	        method : 'GET',
	        url    : url,
	        headers: {
	          'Content-Type' : 'application/json'
	        }
	      };
      return $http(config).then(callback, error);
    };
}]);
