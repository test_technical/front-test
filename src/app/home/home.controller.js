
angular.module('app.home')

.controller('HomeCtrl', ['$scope', 'TheaterService','$rootScope','MoviesService', function($scope,TheaterService,$rootScope,MoviesService) {

		$scope.theaters = [];
		$scope.movies = [];

  		TheaterService.get(function(response){
			$scope.theaters = response.data;
	  		for (var i = 0; i < $scope.theaters.length; i++) { 
			  	if($scope.theaters[i].movies.length > 0){	
			  		for (var j =0; j < $scope.theaters[i].movies.length; j++) {
			  			 MoviesService.getOne($scope.theaters[i].movies[j],function(resp){
			  			 	$scope.movies.push(resp.data)
					  		$scope.MoviesFilter()
		  		      });
			  		}
			  	}
	  		}	
  		});
	
	$scope.MoviesFilter = function(){
	  	for(var i = 0; i < $scope.theaters.length; i++){
			for (var j =0; j < $scope.theaters[i].movies.length; j++) {

				for (var k = 0; k < $scope.movies.length; k++) {
						if($scope.theaters[i].movies[j] == $scope.movies[k]._id){
						$scope.theaters[i].movies[j] = $scope.movies[k]

						}
					}
			}
		}
		console.log($scope.theaters)
	}	


  	
}]);
