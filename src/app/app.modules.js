
  angular.module('app', 
                 [
                   'ngRoute',
                   'templates',
                   'angular-loading-bar', 
                   'xeditable',
                   'app.movies',
                   'app.theaters',
                   'app.home',
                 ]);

