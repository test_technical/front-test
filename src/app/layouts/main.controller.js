angular.module('app.home')

.controller('MainCtrl', ['$scope', '$rootScope','$location',
  function($scope, $rootScope,$location) {
    console.warn('main ctrl');
    $scope.expandedMenu = true;
     $scope.checkPath = function(path){
      return $location.path().indexOf(path) != -1;
      }
    $scope.toogleMenu = function(){
      $scope.expandedMenu = !$scope.expandedMenu;

      if ($scope.expandedMenu){
        $.AdminLTE.pushMenu.expand()
      } else {
        $.AdminLTE.pushMenu.collapse();
      }
    }
}]);
